export interface OAuth2ClientVO extends BaseEntity {
  id: string | number | undefined;
  clientId: string;
  clientSecret: string;
  clientName: string;
  isCode: string;
  isImplicit: string;
  isPassword: string;
  isClient: string;
  allowUrl: string;
  contractScope: string;
  accessTokenTimeout: string;
  refreshTokenTimeout: string;
  clientTokenTimeout: string;
  status: string;
}

export interface OAuth2ClientQuery extends PageQuery {
  clientId: string;
  clientName: string;
}

export interface OAuth2ClientForm {
  id: string | number | undefined;
  clientId: string;
  clientSecret: string;
  clientName: string;
  isCode: string;
  isImplicit: string;
  isPassword: string;
  isClient: string;
  allowUrl: string;
  contractScope: string;
  accessTokenTimeout: string;
  refreshTokenTimeout: string;
  clientTokenTimeout: string;
  status: string;
}
