import request from '@/utils/request';
import { OAuth2ClientQuery, OAuth2ClientVO, OAuth2ClientForm } from './types';
import { AxiosPromise } from 'axios';

// 查询OAuth2.0终端列表
export function listOauth2Client(query: OAuth2ClientQuery): AxiosPromise<OAuth2ClientVO[]> {
  return request({
    url: '/system/oauth2/client/list',
    method: 'get',
    params: query
  });
}

// 查询OAuth2.0终端详细
export function getOauth2Client(id: string | number): AxiosPromise<OAuth2ClientVO> {
  return request({
    url: '/system/oauth2/client/' + id,
    method: 'get'
  });
}

// 新增OAuth2.0终端
export function addOauth2Client(data: OAuth2ClientForm) {
  return request({
    url: '/system/oauth2/client',
    method: 'post',
    data: data
  });
}

// 修改OAuth2.0终端
export function updateOauth2Client(data: OAuth2ClientForm) {
  return request({
    url: '/system/oauth2/client',
    method: 'put',
    data: data
  });
}

// 删除OAuth2.0终端
export function delOauth2Client(id: string | number) {
  return request({
    url: '/system/oauth2/client/' + id,
    method: 'delete'
  });
}
